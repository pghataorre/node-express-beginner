const filterPosition = (teamPlayerIndex, playerPosition) => {
  return playerPosition.filter(positionModelIndex => {
    if (positionModelIndex.positionKey === teamPlayerIndex.position) {
      return positionModelIndex.position;
    }
  });
}

const hasPlayerContent = (teamsObj, id) => {
  if (teamsObj[id]) {
    return teamsObj[id].players.length > 0 ? true : false;
  }

  return false;
} 

module.exports = {
  filterPosition,
  hasPlayerContent
};