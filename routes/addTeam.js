var express = require('express');
var router = express.Router();
var {leagueModel, team} = require('../model/model');

router.get('/', function(req, res, next) {
  res.render('addTeam', {});
});

router.post('/', function(req, res, next) {
  const teamName = req.body.team;

  if (teamName) {
    team.name = teamName;
    leagueModel.teams.push(team);
  }
  res.redirect('/');
});

module.exports = router;