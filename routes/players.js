var express = require('express');
var router = express.Router();
var {leagueModel, teamPlayer} = require('../model/model');

router.get("/:teamId/:playerId", function(req, res, next) {
  const teamId = req.params.teamId;
  const playerId = req.params.playerId;
  const playerDetailObject = leagueModel.teams[teamId].players[playerId];
  const teamObject = leagueModel.teams[teamId];

  res.render("players", { playerDetailObject, leagueModel, teamObject });
});

module.exports = router;