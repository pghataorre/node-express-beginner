var express = require('express');
var router = express.Router();
var { leagueModel, teamPlayer, playerPosition} = require("../model/model");

router.get('/', function(req, res, next) {
  res.render("addPlayer", { leagueModel, playerPosition });
});

router.post('/', function(req, res, next) {

  const teamId = parseInt(req.body.teamId);
  if (teamId !== -1) {
    const tempPlayerObj = teamPlayer;
    const firstName = req.body.playerFirstName;
    const lastName = req.body.playerLastName;
    const playerPosition = req.body.playerPosition;
    const shirtNumber = req.body.playerShirtNo;

    tempPlayerObj.name = firstName;
    tempPlayerObj.lastName = lastName;
    tempPlayerObj.position = playerPosition;
    tempPlayerObj.shirtNumber = shirtNumber;

    leagueModel.teams[teamId].players.push(tempPlayerObj);
  }

  res.render("addPlayer", { playerAdded: true });
  return;
});

module.exports = router