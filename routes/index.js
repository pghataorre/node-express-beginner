var express = require('express');
var router = express.Router();
var model = require('../model/model');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', model.leagueModel);
});

module.exports = router;
