var express = require('express');
var router = express.Router();
var handleResponse = require("../controllers/playerListController");
var { leagueModel, playerPosition } = require("../model/model");


router.get("/", function(req, res, next) {
  res.render("playerList", { leagueModel, hasPlayers: false });
  return;
});

router.get("/:id", function(req, res, next) {
  handleResponse(req, res, next);
  return;
});
  
router.post("/", function(req, res, next) {
  handleResponse(req, res, next);
});

module.exports = router;
