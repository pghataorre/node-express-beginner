var { filterPosition, hasPlayerContent } = require("../services/common");
var { leagueModel, playerPosition } = require("../model/model");

function handleResponse(req, res, next) {
  if (req.params.id) {
    id = req.params.id;
  } else {
    id = req.body.teamId;
  }
   
  if (id === null) {
    res.render("playerList", { leagueModel, hasPlayers: false });
    return;
  }

  const havePlayers = hasPlayerContent(leagueModel.teams, id);
  if (!havePlayers) {
    res.render("playerList", { leagueModel, hasPlayers: false });
    return;
  }

  const teamPlayers = leagueModel.teams[id];
  const teamPlayersPosisitonFiltered = teamPlayers.players.map(
    teamPlayerIndex => {
      const filteredPosition = filterPosition(teamPlayerIndex, playerPosition);
      teamPlayerIndex.stringPlayerPosition = filteredPosition[0].position;
      return teamPlayerIndex;
    }
  );

  teamPlayers.players = teamPlayersPosisitonFiltered;
  res.render("playerList", {
    teamId: parseInt(id),
    name: leagueModel.teams[id].name,
    teamBadge: leagueModel.teams[id].teamBadge,
    leagueModel,
    teamPlayers,
    hasPlayers: true
  });
  return;
}

module.exports = handleResponse;