const leagueModel = {
  teams: [
    {
      name: "Manchester City",
      teamBadge: 'manchestercity',
      players: [
        {
          name: "Sergio",
          lastName: "Aguero",
          shirtNumber: 1,
          position: "STK"
        },
        {
          name: "Leroy",
          lastName: "Sane",
          shirtNumber: 19,
          position: "STK"
        },
        {
          name: "Riyad",
          lastName: "Mahrez",
          shirtNumber: 19,
          position: "STK"
        }
      ]
    },
    {
      name: "Liverpool FC",
      teamBadge: 'liverpool',
      players: []
    },
    {
      name: "Arsenal FC",
      teamBadge: 'arsenal',
      players: []
    },
    {
      name: "West Ham Utd",
      teamBadge: 'westham',
      players: []
    },
    {
      name: "Manchester Utd",
      teamBadge: 'manchesterunited',
      players: []
    },
    {
      name: "Wankers",
      teamBadge: 'wankers',
      players: []
    },
    {
      name: "Norwich City",
      teamBadge: 'norwich',
      players: []
    },
    {
      name: "Wolverhampton",
      teamBadge: 'wolverhampton',
      players: []
    },
    {
      name: "Aston Villa FC",
      teamBadge: 'astonvilla',
      players: []
    },
    {
      name: "Leicester City FC",
      teamBadge: 'leicester',
      players: []
    },
    {
      name: "Chelsea FC",
      teamBadge: 'chelsea',
      players: []
    },
    {
      name: "Sheffield Utd",
      teamBadge: 'sheffieldutd',
      players: []
    },
    {
      name: "Bournmouth",
      teamBadge: 'bournmouth',
      players: []
    },
    {
      name: "Brighton",
      teamBadge: 'brighton',
      players: []
    },
    {
      name: "Watford",
      teamBadge: 'watford',
      players: []
    },
    {
      name: "Everton",
      teamBadge: 'everton',
      players: []
    },
    {
      name: "Crystal Palace",
      teamBadge: 'crystalpalace',
      players: []
    },
    {
      name: "Southampton FC",
      teamBadge: 'southampton',
      players: []
    },
    {
      name: "Burnley FC",
      teamBadge: 'burnley',
      players: []
    }
  ]
};

const team = {
  name: 'The rest',
  players: []
}

const teamPlayer = {
  name: null,
  lastName: null,
  shirtNumber: null,
  position: null
};

const playerPosition = [
    { position: "Striker", positionKey: "STK" },
    { position: "Attacking Midfielder", positionKey: "AMID" },
    { position: "Midfielder", positionKey: "MID" },
    { position: "Left Winger", positionKey: "LW" },
    { position: "Right Winger", positionKey: "RW" },
    { position: "Left back", positionKey: "LB" },
    { position: "Right Back", positionKey: "RB" },
    { position: "Center Back", positionKey: "CB" },
    { position: "Goal Keeper", positionKey: "GK" }
  ];

module.exports = {
  leagueModel,
  teamPlayer,
  team,
  playerPosition
};